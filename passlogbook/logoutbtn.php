<?php

    include("db.php");
    include("header.php");
?>
<?php
	session_start();
    $admin_user = $_SESSION['username'];
    $admin_pass = $_SESSION['password'];

    if(isset($admin_user) && isset($admin_pass)) {
        unset($_SESSION['username']);
        unset($_SESSION['password']);
		session_destroy();
        mysqli_close($conn);
        header("Location: index.php");
    }
	else{
		header("Location: index.php");
		 exit();
	}
	if($_SESSION['status']!="Active")
	{
		header("location:index.php");
	}
?>