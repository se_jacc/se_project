 <?php
    // 1. Create a database connection
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "databaseproject";
    $connection = mysqli_connect($dbhost, $dbuser, $dbpass,  $dbname);

    // Test if connection occurred. 
    if(mysqli_connect_errno()) {
        die("Database connection failed: " . 
            mysqli_connect_error() . 
            " (" . mysqli_connect_errno() . ")"
        );
    }
?>

<?php
	session_start();
    $user="";
    $pass="";
    
    if(isset($_POST['login'])){
            $password = $_POST['password'];
			
			if(isset($_POST['username'])) {
				$username = $_POST['username'];
			}
			
			if(isset($_POST['password'])) {
				$password = $_POST['password'];
			}

			if($_POST['username'] === '' || $_POST['password'] === '') {
				$message = "Username and/or Password incorrect.\\nTry again.";
				echo "<script type='text/javascript'>alert('$message');</script>";
				header("Location: login.php");
			}else {
				$query = "SELECT * FROM admin where username = '".$username."' AND password = '".$password."';";
				echo $query;
				$result = mysqli_query($connection, $query);
				while($row = mysqli_fetch_assoc($result)) {
					$user = $row['username'];
					$pass = $row['password'];
				}
				
				if($result){
					$_SESSION['username'] = $username; 
					$_SESSION['password'] = $password;
					
					if(($user === $username) and ($pass === $password)){
						header("Location: index.php");
					}else {
						header("Location: login.php");
					}					
				}else{
					die("Database Query Failed!".mysqli_error($connection));
				}
			}
         mysqli_free_result($result); 
    }
  ?>


<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="styles/bootstrap.min.css">
        <link rel="stylesheet" href="styles/style.css?ver=1.0.1"> 
        <link rel="stylesheet" href="styles/bootstrap.css">
        <link rel="stylesheet" href="styles/bootstrap.min.css">
        <link rel="stylesheet" href="styles/bootstrap-theme.css">
        <link rel="stylesheet" href="styles/bootstrap-theme.min.css">

        <div id="visitors-container">
               <img src="Images/Useplogo.png" alt="Use data to see photos" style="position: absolute; height: 130px; width: 130px;margin-top: 10px; margin-left: 320px;">
                <h3 style="font-family: serif; color: whitesmoke;font-size: 30px;display: flex; justify-content: center;margin-top: 25px">University of Southeastern Philippines</h3>
                <h1 style="font-family: serif; color: whitesmoke;font-size: 45px; display: flex; justify-content: center; margin-top: 8px; ">Visitor's Log Book</h1>
        </div>
    </head>
    <body>
       
     <div id="content" style="background-color:lavenderblush;" class="col-sm-12">
        <div class="container-fluid">
            <h2>Admin Login</h2>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" id="login-group">
                <div class="form-group">
                    <label for="username">Username: </label>
                    <input type="text" class="form-control" name="username" id="username"  >
                </div>
                <div class="form-group">
                    <label for="password">Password: </label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>
                <button type="submit" class="btn btn-danger"  name="login" onClick="return confirm('Are you sure you want to login?')" >Login</button>
                <span><a href="adminlogin.php">Settings</a></span>
            </form>
        </div>
    </div>
    </body>
    

</html>
<?php
    // 5. Close database connection
    mysqli_close($connection);
?>
